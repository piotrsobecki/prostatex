# README #

Whatever
### What is this repository for? ###

* PW MINI: ProstateX

### How do I get set up? ###

* Download ProstateX data
* Enforce correct directory structure

### Directory structure ###

/{project.dir}/data/ProstateX/train/DOI

/{project.dir}/data/ProstateX/train/ktrans

/{project.dir}/data/ProstateX/train/lesion-information/

/{project.dir}/data/ProstateX/train/lesion-information/

/{project.dir}/data/ProstateX/train/lesion-information/ProstateX.csv (if absent generated automatically by [Dataset](src/dataset.py))
\- a dataset file containing data merged from patients mhd files and meta-data files (csv)

/{project.dir}/data/ProstateX/train/lesion-information/ProstateX-DataInfo-Test.docx

/{project.dir}/data/ProstateX/train/lesion-information/ProstateX-Findings.csv

/{project.dir}/data/ProstateX/train/lesion-information/ProstateX-Images.csv

/{project.dir}/data/ProstateX/train/lesion-information/ProstateX-Images-KTrans.csv

/{project.dir}/data/ProstateX/train/screenshots (optional)