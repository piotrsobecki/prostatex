# # Show sample finding

import sys
import pandas as pd
from matplotlib import pyplot, cm, gridspec
import matplotlib.image as mpimg
from numpy import *
import os
from distutils.util import strtobool

sys.path.append('../src')
import prostatex



def get_fig(row,dcm):
    ijk = row['ijk'].split(' ')
    x = int(ijk[0])
    y = int(ijk[1])
    z = int(ijk[2])
    clinSig = strtobool(str(row['ClinSig']))
    dc='g'
    if clinSig:
        dc = 'r'
    finding_screenshot = mpimg.imread(screenshots_dir + prostatex.getScreenshotFileName(row))
    dcm_t = array(dcm[:, :,z]).T
    dcm_shape = dcm.shape
    dpi = 20
    fig = pyplot.figure(figsize=(dcm_shape[0]/dpi, 2*dcm_shape[1]/dpi), dpi=dpi)
    pyplot.subplot2grid((1,3), (0,0))
    pyplot.title('DCM')
    pyplot.imshow(dcm_t, cmap='Greys_r')
    pyplot.axis('off')
    pyplot.subplot2grid((1,3), (0,1))
    pyplot.title('Finding')
    pyplot.scatter([x], [y], s=[0.1 * dcm_shape[1]], c=dc)
    pyplot.imshow(dcm_t, cmap='Greys_r')
    pyplot.axis('off')
    pyplot.subplot2grid((1,3), (0,2))
    pyplot.title('Reference')
    pyplot.imshow(finding_screenshot, cmap='Greys_r')
    pyplot.axis('off')
    return fig

def get_fig_region(row,dcm):
    ijk = row['ijk'].split(' ')
    x = int(ijk[0])
    y = int(ijk[1])
    z = int(ijk[2])
    clinSig = strtobool(str(row['ClinSig']))
    dc='g'
    if clinSig:
        dc = 'r'
    finding_screenshot = mpimg.imread(screenshots_dir + prostatex.getScreenshotFileName(row))
    dcm_r = prostatex.region3d(dcm,x,y,int(dcm.shape[0]/4))
    dcm_t = array(dcm_r[:, :,z]).T
    x = dcm_t.shape[0]/2
    y = dcm_t.shape[1]/2
    dcm_shape = dcm.shape
    dpi = 20
    fig = pyplot.figure(figsize=(dcm_shape[0]/dpi, 2*dcm_shape[1]/dpi), dpi=dpi)
    pyplot.subplot2grid((1,3), (0,0))
    pyplot.title('DCM')
    pyplot.imshow(dcm_t, cmap='Greys_r')
    pyplot.axis('off')
    pyplot.subplot2grid((1,3), (0,1))
    pyplot.title('Finding')
    pyplot.scatter([x], [y], s=[0.1 * dcm_shape[1]], c=dc)
    pyplot.imshow(dcm_t, cmap='Greys_r')
    pyplot.axis('off')
    pyplot.subplot2grid((1,3), (0,2))
    pyplot.title('Reference')
    pyplot.imshow(finding_screenshot, cmap='Greys_r')
    pyplot.axis('off')
    return fig


def savefig(fig,out_dir_file, fname):
    if not os.path.exists(out_dir_file):
        os.makedirs(out_dir_file)
    fig.savefig(os.path.join(out_dir_file,fname), bbox_inches='tight')  # save the figure to file
    pyplot.close(fig)  # close the figure


def mark_all_findings(df):
    df_err = pd.DataFrame(columns=df.columns)
    for index,row in df.iterrows():
        try:
            proxId = row['ProxID']
            name = row['Name']
            fid = row['fid']
            seqName = str(row['DCMSeqName'])
            if seqName.startswith('*'):
                seqName = seqName[1:]
            clinSig = row['ClinSig']
            if clinSig:
                clinSigStr  = 'Significant'
            else:
                clinSigStr = 'Not Significant'
            dcmOffset = int(row['DCMSerOffset'])
            dcmLen = int(row['Dim'].split('x')[2])
            dcmSerNum = row['DCMSerNum']
            dir = rel+row['DCMSerDir']
            dcm,dss,seqNames = prostatex.load_dcm(dir, dcmOffset, dcmLen)
            fig = get_fig(row,dcm)
            savefig(fig,os.path.join(out_dir,seqName,clinSigStr),"%s-Finding%d-%s-%d.png"%(proxId,fid,name,dcmSerNum))
            fig = get_fig_region(row,dcm)
            savefig(fig,os.path.join(out_dir,seqName,clinSigStr),"%s-Finding%d-%s-%d_r.png"%(proxId,fid,name,dcmSerNum))
            pyplot.close(fig)  # close the figure
        except IndexError as e:
            print("IndexError when constructing series array")
            print(e)
            print(row)
            df_err = df_err.append(row)
    df_err.to_csv(err_out)

rel = '../'
train_file = rel+"data/ProstateX/train/leasion-information/ProstateX-train.csv"
screenshots_dir = rel+"data/ProstateX/train/screenshots/"
out_dir = rel+"target/findings-test/"
err_out = out_dir +"err.csv"

if __name__ == '__main__':
    df = pd.read_csv(train_file,sep=',')
    #df=df[df['ProxID']=='ProstateX-0199']
    mark_all_findings(df)