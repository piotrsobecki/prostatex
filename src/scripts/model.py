from os.path import join, dirname
import json
from sklearn.neighbors import KNeighborsClassifier
from prostatex.dataset import DataSet

# Configure logger
from prostatex.scripts.optimization import OptimizationContext


version = "05_04_17"
cls = [KNeighborsClassifier(n_neighbors=n) for n in [1, 3, 5]]
conf = json.load(open(join(dirname(__file__), "model.json")))

ctx = OptimizationContext(
    base_dir="log/Models",# Data directory
    dataset=DataSet(base_dir="data/ProstateX/train/"), # Configure dataset (provides parsed data)
    base_settings=dict(ngen=1000, indpb=0.05, sep=";", n_max=500)
)


ctx.do_extract_features(version, extractor_settings=conf)
ctx.do_test_all(version, cls=cls)
ctx.do_optimize_whole(version, cls=cls)