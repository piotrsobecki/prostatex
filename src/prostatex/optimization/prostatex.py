import sys
import numpy as np
import pandas as pd
from os.path import join
from prostatex.utils.utils import *
from sklearn.metrics import roc_curve, auc

def roc_auc_score(y_test,y_score):
    fpr, tpr, _ = roc_curve(y_test, y_score)
    return auc(fpr, tpr)

def prostatex_auc(labels, predictions):
    return roc_auc_score(labels, predictions[:, 1])

def get_optimizer(optimizerClass, clf, settings):
    settings = {'datalog': 'datalog.csv', 'genlog': 'genlog.csv', 'base_dir': './', **settings, **args_map(sys.argv)}
    settings['datalog'] = join(settings['base_dir'],settings['datalog'])
    settings['genlog'] = join(settings['base_dir'],settings['genlog'])
    backup_file(settings['datalog'])
    backup_file(settings['genlog'])
    features,labels = read_features_labels(settings)
    return optimizerClass(clf, features=features, labels=labels, score_func=prostatex_auc, **settings)



def split_features(features):
    labels = read_labels(features)
    features = features.drop('clinsig', 1)
    return features,labels

def read_features_labels(settings):
    features = pd.read_csv(settings['data'], sep=settings['sep'], index_col=None, na_values=['', 'na', 'nan', 'NaN'])
    features = features.apply(lambda x: x.astype(np.float32))
    labels = read_labels(features)
    features = features.drop('clinsig', 1)
    return features,labels.to_frame('clinsig').apply(lambda x: x.astype(np.int))

def read_labels(features):
    return features['clinsig']